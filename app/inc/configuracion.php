<?php

class Configuracion {
	public $db_host = 'localhost';
	public $db_nombre = 'ticket';
	public $db_usuario = 'root';
	public $db_contrasena = '';

	public function db_host() {
		return $this->db_host;
	}
	public function db_nombre() {
		return $this->db_nombre;
	}
	public function db_usuario() {
		return $this->db_usuario;
	}
	public function db_contrasena() {
		return $this->db_contrasena;
	}
}

?>