<?php require_once('vistas/vista.encabezado.php');
session_start();
if (!$_SESSION['contrasena_actualizada']) {
	session_destroy();
	header('Location: ingreso');
} else {}
?>

<div class="container" style="margin-top: 25px;">
<div class="row">
<div class="col-md-12">
<div class="panel panel-default">
<div class="panel-body">
<center><h4>Tu contraseña ha sido actualizada correctamente!</h4></center>
<center>
<form method="POST" action="salir"><div class="btn-group">
<input type="submit" id="no" name="no" class="btn btn-success center-block" value="Vale, volver ahora.">
</div></form></p></center>

</div>
</div>
</div>

</div>
</div>
<?php require_once('vistas/vista.src.php'); ?>