<?php require_once('vistas/vista.encabezado.php'); require_once('controladores/c.funciones.php'); general::base_url(); general::verificar_usuario(); require_once('controladores/c.perfil.php');
require_once('vistas/vista.menu.php'); ?>

<div class="container" style="margin-top: 25px;">

<?php 
$usuario = new perfil($_SESSION['id_usuario_sistema']); 
if (isset($_POST['informacion'])) {
$actualizar = new actualizar_datos($_SESSION['id_usuario_sistema'], $f_nombre = $_POST['nombre'], $f_apellidos = $_POST['apellidos']); $actualizar->verificar_datos(); $actualizar->verificar_igualadad(); $actualizar->actualizar();
} else {} 

if (isset($_POST['contrasena'])) {
$actualizar_c = new actualizar_contrasena($_SESSION['id_usuario_sistema'], $f_contrasena_a = $_POST['contrasena_actual'], $f_contrasena_n = $_POST['contrasena_nueva'], $f_contrasena_n_r = $_POST['contrasena_nueva_r']);
$actualizar_c->verificar_datos();
$actualizar_c->actualizar();
} else {}
?>

<div class="row">
<div class="col-md-12">
<div class="panel panel-default">
<div class="panel-heading"><h4><strong>Mi perfil</strong></h4></div>
<div class="panel-body">
<?php if (isset($_GET['error'])): ?>
	<div class="alert alert-danger"><strong>Oops!</strong> Al parecer tus datos son erroneos!</div>
<?php endif ?>
<form method="POST" action="perfil">
<p><label>Nombre: </label>
<input type="text" name="nombre" id="nombre" value="<?php echo $usuario->nombre(); ?>" class="form-control" autocomplete="off" required="required"></p>
<p><label>Apellidos: </label>
<input type="text" name="apellidos" id="apellidos" value="<?php echo $usuario->apellidos(); ?>" class="form-control" autocomplete="off" required="required"></p>
<p><label>Correo electroníco: </label>
<input type="email" class="form-control" value="<?php echo $usuario->correo(); ?>" disabled=""></p>
<input type="submit" name="informacion" id="informacion" class="btn btn-success" value="Actualizar información">
</form>
<form method="POST" action="perfil">
<hr>
<?php if (isset($_GET['a'])): ?>
	<div class="alert alert-danger"><strong>Oops!</strong> Al parecer tu contraseña actual no coincide!</div>
<?php endif ?>

<?php if (isset($_GET['n'])): ?>
	<div class="alert alert-danger"><strong>Oops!</strong> Al parecer tu nueva contraseña no coincide con la de verificación!</div>
<?php endif ?>
<h3>Cambiar contraseña</h3>
<p><label>Contraseña actual: </label>
<input type="password" name="contrasena_actual" id="contrasena_actual" class="form-control" required=""></p>
<p><label>Nueva contraseña: </label>
<input type="password" name="contrasena_nueva" id="contrasena_nueva" class="form-control" required=""></p>
<p><label>Repetir contraseña: </label>
<input type="password" name="contrasena_nueva_r" id="contrasena_nueva_r" class="form-control" required=""></p>
<input type="submit" name="contrasena" id="contrasena" class="btn btn-success" value="Actualizar contraseña">
</form>
</div>
</div>
</div>
</div>
</div>
<?php require_once('vistas/vista.src.php'); ?>