<?php require_once('vistas/vista.encabezado.php'); require_once('controladores/c.funciones.php'); general::base_url(); general::verificar_entrada(); require_once('controladores/c.ingreso.php'); ?>

<?php if (isset($_POST['ingreso'])) {
$logear = new ingreso();
$logear->verificar_datos($f_correo = $_POST['f_correo'], $f_contrasena = $_POST['f_contrasena']);
$logear->verificar();
} else {} ?>

<section class="section">
<div class="container">
<div class="card bordered z-depth-2" style="margin:0 auto; max-width:400px;">
<div class="card-header">
<span class="card-title">Ingreso</span>
</div>
<div class="card-content">
<?php if (isset($_GET['error'])): ?>
<div class="alert alert-danger"><strong>Ha ocurrido un error!</strong> Datos incorrectos!</div>
<?php endif ?>
<form role="form" method="POST" action="ingreso">
<div class="input-group">
<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
<input type="text" class="form-control" placeholder="Correo" id="f_correo" name="f_correo" required="">
</div>
<br />
<div class="input-group">
<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
<input type="password" class="form-control" placeholder="Contraseña" id="f_contrasena" name="f_contrasena" required="">
</div>
<br />
<a href="registro"><p>¿No tienes cuenta?</p></a>
</div>
<div class="card-action clearfix">
<div class="pull-right">
<a href="r-paso1" class="btn btn-link text-primary">Recuperar contraseña</a>
<input type="submit" id="ingreso" name="ingreso" value="Ingresar" class="btn btn-default">
</div>
</div>
</form>
</div>
</div>
</section>

</body>
</html>