<?php require_once('vistas/vista.encabezado.php'); require_once('controladores/c.funciones.php'); general::verificar_usuario(); require_once('vistas/vista.menu.php'); require_once('controladores/paginador.tickets.php'); ?>

<div class="container" style="margin-top: 25px;">
<div class="row">
<div class="col-md-12">
<div class="panel panel-default">
<div class="panel-heading"><h4><strong>Mis tickets</strong></h4></div>
<div class="panel-body">

<?php if (!$tickets): ?>
<div class="alert alert-danger">
<strong>Oops!</strong> Al parecer no hay nada aqui!
</div>
<?php endif ?>

<?php if ($tickets): ?>
<table class="table table-hover">
<tr>
<thead>
<td><strong>#</strong></td>
<td><strong>Nombre</strong></td>
<td><strong>Creado</strong></td>
<td><strong>Estado</strong></td>
<td><strong> </strong></td>
</thead>
</tr>
<tr>
<?php foreach ($tickets as $datos): ?>
<td><?php echo $datos['numero'] ?></td>
<td><?php echo $datos['nombre'] ?></td>
<td><?php echo $datos['fecha'] ?></td>
<td><?php $estado = new Estado($datos['id_estado']); $estado->propiedad(); ?></td>
<td><a href="ticket?id=<?php echo $datos['id_tiquete']; ?>"><input type="submit" value="Ver" class="btn btn-primary btn-sm"></a></td>
</tr>
<?php endforeach ?>
</table>

<ul class="pagination">
<?php if ($pagina == 1): ?>
<li class="disabled"><a href="#">&laquo;</a></li>
<?php else: ?>
<li><a href="?pagina=<?php echo $pagina - 1 ?>">&laquo;</a></li>
<?php endif; ?>

<?php 
for ($i=1; $i <= $numeroPaginas ; $i++) { 
	if ($pagina == $i) {
		echo "<li class='active'><a href='?pagina=$i'>$i</a></li>";
	} else {
		echo "<li><a href='?pagina=$i'>$i</a></li>";
	}
}
?>
<?php endif ?>

</ul>
</div>
</div>
</div>
</div>
</div>
<?php require_once('vistas/vista.src.php'); ?>