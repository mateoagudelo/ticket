<?php require_once('vistas/vista.encabezado.php'); require_once('controladores/c.funciones.php'); general::base_url(); general::verificar_entrada(); require_once('controladores/c.ingreso.php'); require_once('controladores/c.recuperar.php'); ?>


<?php

if (!$_SESSION['correo_usuario_recuperar'] AND !$_SESSION['id_usuario_recuperar']) {
	session_destroy();
	header('Location: recuperar');
} else {}

if (isset($_POST['cambiar_contrasena'])) {
	session_start();
	$cambiar = new cambiar_contrasena($_SESSION['codigo_usuario_recuperar'], $_SESSION['id_usuario_recuperar'], $contrasena = $_POST['contrasena'], $r_contrasena = $_POST['r_contrasena']);
	$cambiar->verificar();
	$cambiar->cambiar();
} else {}

?>

<section class="section">
<div class="container">
<div class="card bordered z-depth-2" style="margin:0 auto; max-width:400px;">
<div class="card-header">
<span class="card-title">Paso 3: Cambiar contraseña</span>
</div>
<div class="card-content">
<form role="form" method="POST" action="r-paso3">
<div class="input-group">
<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
<input type="password" class="form-control" placeholder="Contraseña nueva" id="contrasena" name="contrasena" required="">
</div>
</br>
<div class="input-group">
<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
<input type="password" class="form-control" placeholder="Repetir Contraseña" id="r_contrasena" name="r_contrasena" required="">
</div>
</div>

<div class="card-action clearfix">
<div class="pull-right">
<input type="submit" id="cambiar_contrasena" name="cambiar_contrasena" value="Confirmar" class="btn btn-success">
</form>
<input type="submit" value="Cancelar" class="btn btn-danger">
</div>
</div>
</div>
</div>
</section>
</body>