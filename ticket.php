<?php require_once('vistas/vista.segura.encabezado.php'); require_once('controladores/c.funciones.php'); general::verificar_usuario(); require_once('vistas/vista.menu.php'); require_once('controladores/c.tickets.php'); require_once('controladores/c.mensajes.php'); ?>

<?php
$id = $_GET['id'];

$men = new Mensajes($id); 

$t = new Ticket($_SESSION['id_usuario_sistema'], $id);
$t->Verificar_entrada();
$t->get_datos();

if (isset($_POST['cambiar'])) {
	$t->cerrar_estado();
} elseif (isset($_POST['eliminar'])) {
	$t->eliminar_ticket();
}

if (isset($_POST['responder'])) {
	$men->mensaje_usuario($_SESSION['id_usuario_sistema'], $mensaje = $_POST['mensaje']);
}

?>

<div class="container" style="margin-top: 25px;">
<div class="row">
<div class="col-md-8">
<div class="panel panel-default">
<div class="panel-heading"><h4><strong><?php $t->Nombre(); ?></strong></h4></div>
<div class="panel-body">
<?php $t->Descripcion(); ?>
</div>
</div>

<?php $men->get_mensajes(); ?>

<?php if ($t->estado_verificado != 3): ?>
<div class="panel panel-default">
<div class="panel-heading"><h4><strong>Agregue una respuesta: </strong></h4></div>
<div class="panel-body">

<form method="POST" action="ticket?id=<?php $t->Id_ticket(); ?>">
	<textarea name="mensaje" id="mensaje" class="form-control" rows="5"></textarea>
	<br>
	<input type="submit" class="btn btn-success" value="Responder" name="responder" id="responder">
</form>

</div>
</div>
<?php endif ?>

</div>

<div class="col-md-4">
<div class="panel panel-default">
<div class="panel-body">
<p><strong>Estado:</strong> <?php $t->Estado(); ?> </p>
<p><strong>Creado:</strong> <?php $t->Creado(); ?></p> 
<p><strong>Departamento:</strong> <?php $t->Departamento(); ?></p>
<p><strong>Producto:</strong> <?php $t->Producto(); ?></p>

<?php if ($t->Archivo() != NULL): ?>
<p><strong>Archivo adjunto:</strong></p><p><a href="#"><input type="submit" class="btn btn-success btn-lg btn-block" value="Descargar"></a></p>
<?php endif ?>

</div>
</div>
<?php if ($t->estado_verificado != 3): ?>
<p><form method="POST" action="ticket?id=<?php $t->Id_ticket(); ?>"><input type="submit" name="cambiar" id="cambiar" class="btn btn-warning btn-lg btn-block" value="Cerrar solicitud"></form></p>
<?php endif ?>

<p><form method="POST" action="ticket?id=<?php $t->Id_ticket(); ?>"><input type="submit" name="eliminar" id="eliminar" class="btn btn-danger btn-lg btn-block" value="Eliminar solicitud"></form></p>
</div>
</div>
</div>
<?php require_once('vistas/vista.src.php'); ?>