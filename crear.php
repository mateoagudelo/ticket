<?php require_once('vistas/vista.encabezado.php'); require_once('controladores/c.funciones.php'); general::base_url(); general::verificar_usuario(); 
require_once('vistas/vista.menu.php'); require_once('controladores/c.crear.php'); $des = new Seleccion(); ?>

<?php if (isset($_POST['crear'])) {
	$crear = new Crear($_SESSION['id_usuario_sistema'], $nombre = $_POST['nombre'], $descripcion = $_POST['descripcion'], 	$departamento = $_POST['departamento'], $producto = $_POST['producto'], $prioridad = $_POST['prioridad']);
	$crear->insertar_datos();
} else {}
?>

<div class="container" style="margin-top: 25px;">
<div class="row">
<div class="col-md-8">
<div class="panel panel-default">
<div class="panel-heading"><h4><strong>Crear nuevo tiquete de soporte</strong></h4></div>
<div class="panel-body">
<form method="POST" action="crear" enctype="multipart/form-data">
<p><label>Nombre: </label>
<input type="text" name="nombre" id="nombre" class="form-control"></p>
<p><label>Descripción: </label>
<textarea id="descripcion" name="descripcion" rows="10" cols="50">...</textarea></p>
<hr>

<p><label>Departamento: </label>
<?php $des->departamentos(); ?> 
</p>

<p><label>Producto: </label>
<?php $des->productos(); ?> 
</p>

<label>Prioridad: </label>
<?php $des->prioridad(); ?> 
</p>

<p><label>Archivo: </label>
<input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
<input type="file" class="filestyle" name="archivo_sistema"></p>

<input type="submit" name="crear" id="crear" class="btn btn-success" value="Crear">
</form>
</div>
</div>
</div>
<div class="col-md-4">
<div class="panel panel-default">
<div class="panel-heading"><h4><strong>Noticias</strong></h4></div>
<div class="panel-body">

</div>
</div>
</div>
</div>
</div>

<?php require_once('vistas/vista.src.php'); ?>