-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-10-2016 a las 04:53:23
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ticket`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_admin`
--

CREATE TABLE `t_admin` (
  `id_admin` int(11) NOT NULL,
  `nivel_admin` int(11) NOT NULL,
  `nombre_admin` varchar(100) NOT NULL,
  `apellidos_admin` varchar(100) NOT NULL,
  `correo_admin` varchar(300) NOT NULL,
  `contrasena_admin` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_departamentos`
--

CREATE TABLE `t_departamentos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `descripcion` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `t_departamentos`
--

INSERT INTO `t_departamentos` (`id`, `nombre`, `descripcion`) VALUES
(1, 'Ventas', 'Aqui se vende... '),
(2, 'Soporte', 'Aqui se soporta----');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_estados`
--

CREATE TABLE `t_estados` (
  `id` int(11) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `descripcion` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `t_estados`
--

INSERT INTO `t_estados` (`id`, `nombre`, `descripcion`) VALUES
(1, 'En espera', 'Tu tikcket esta a la espera'),
(2, 'Procesando...', 'Estamos revisando tu solicitud'),
(3, 'Cerrado', 'Tu peticion esta cerrada.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_mensajes`
--

CREATE TABLE `t_mensajes` (
  `id` int(11) NOT NULL,
  `id_tiquete` int(11) NOT NULL,
  `id_super` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `mensaje_super` varchar(2000) NOT NULL,
  `mensaje_usuario` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_prioridad`
--

CREATE TABLE `t_prioridad` (
  `id` int(11) NOT NULL,
  `nombre` varchar(70) NOT NULL,
  `descripcion` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `t_prioridad`
--

INSERT INTO `t_prioridad` (`id`, `nombre`, `descripcion`) VALUES
(1, 'Baja', 'Hola...'),
(2, 'Media', 'Hollaaa'),
(3, 'Alta', 'Holaaa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_productos`
--

CREATE TABLE `t_productos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `descripcion` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `t_productos`
--

INSERT INTO `t_productos` (`id`, `nombre`, `descripcion`) VALUES
(1, 'Script Ruby', 'Holaaaaaaaa'),
(2, 'Camisa', 'Hola....');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_tiquetes`
--

CREATE TABLE `t_tiquetes` (
  `id_tiquete` int(11) NOT NULL,
  `id_creador` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `fecha` varchar(20) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(5000) NOT NULL,
  `id_estado` int(11) NOT NULL DEFAULT '1',
  `id_departamento` int(11) NOT NULL,
  `id_prioridad` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `url_archivo` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `t_tiquetes`
--

INSERT INTO `t_tiquetes` (`id_tiquete`, `id_creador`, `numero`, `fecha`, `nombre`, `descripcion`, `id_estado`, `id_departamento`, `id_prioridad`, `id_producto`, `url_archivo`) VALUES
(11, 3, 78717, '13 del 10 de 2016', 'Hola que tal!!', '<p>Necesito ayuda por favor!!</p>', 1, 2, 3, 1, NULL),
(18, 3, 78717, '13 del 10 de 2016', 'Hola que tal!!', '<p>Necesito ayuda por favor!!</p>', 1, 2, 3, 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_usuarios`
--

CREATE TABLE `t_usuarios` (
  `id_usuario` int(11) NOT NULL,
  `nombre_usuario` varchar(100) NOT NULL,
  `apellidos_usuario` varchar(100) NOT NULL,
  `correo_usuario` varchar(300) NOT NULL,
  `contrasena_usuario` varchar(1000) NOT NULL,
  `codigo_recuperar_contrasena` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `t_usuarios`
--

INSERT INTO `t_usuarios` (`id_usuario`, `nombre_usuario`, `apellidos_usuario`, `correo_usuario`, `contrasena_usuario`, `codigo_recuperar_contrasena`) VALUES
(1, 'Hola', 'Bedoya Agudelo', 'mateobedoyaagudelo@gmail.com', '$2y$12$10gbYAi3eclVloIk7gSaVu9t.a07p78aVoFBUGRqHR60ljdTTPat2', NULL),
(2, 'luisa', 'acevedo', 'luisa@gmail.com', '$2y$12$T.xsreo4brWj7HIzFSBAU.xk0MiqjsLqJJtLrgqp2I87rOVZGBzda', NULL),
(3, 'mateo', 'caracas', 'mateo@gmail.com', '$2y$12$nUIm5tsuEeUW.eR2tZRTieGpQYg6VMGtDFZlalFtkLZCuQL1LhzIW', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `t_admin`
--
ALTER TABLE `t_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indices de la tabla `t_departamentos`
--
ALTER TABLE `t_departamentos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `t_estados`
--
ALTER TABLE `t_estados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `t_mensajes`
--
ALTER TABLE `t_mensajes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `t_prioridad`
--
ALTER TABLE `t_prioridad`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `t_productos`
--
ALTER TABLE `t_productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `t_tiquetes`
--
ALTER TABLE `t_tiquetes`
  ADD PRIMARY KEY (`id_tiquete`);

--
-- Indices de la tabla `t_usuarios`
--
ALTER TABLE `t_usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `t_admin`
--
ALTER TABLE `t_admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `t_departamentos`
--
ALTER TABLE `t_departamentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `t_estados`
--
ALTER TABLE `t_estados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `t_mensajes`
--
ALTER TABLE `t_mensajes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `t_prioridad`
--
ALTER TABLE `t_prioridad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `t_productos`
--
ALTER TABLE `t_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `t_tiquetes`
--
ALTER TABLE `t_tiquetes`
  MODIFY `id_tiquete` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT de la tabla `t_usuarios`
--
ALTER TABLE `t_usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
