<?php require_once('vistas/vista.encabezado.php'); require_once('controladores/c.funciones.php'); general::verificar_usuario(); require_once('vistas/vista.menu.php'); require_once('controladores/c.secciones.php'); ?>

<div class="container" style="margin-top: 25px;">
<div class="row">
<div class="col-md-8">
<div class="panel panel-default">
<div class="panel-heading"><h4><strong>Departamentos</strong></h4></div>
<div class="panel-body">

<table class="table table-hover">
<thead>
<tr>
<td><strong>Nombre</strong></td>
<td><strong>Descripción</strong></td>
</tr>
</thead>
<?php $dep = new Seccion(); $dep->Departamentos(); ?>
</table>

</div>
</div>
</div>
<div class="col-md-4">
<div class="panel panel-default">
<div class="panel-heading"><h4><strong>Noticias</strong></h4></div>
<div class="panel-body">

</div>
</div>
</div>
</div>
</div>
<?php require_once('vistas/vista.src.php'); ?>