<?php require_once('vistas/vista.encabezado.php'); require_once('controladores/c.funciones.php'); general::base_url(); general::verificar_entrada(); require_once('controladores/c.registro.php'); 

if (isset($_POST['registro'])) {
	$registrar_ahora = new registro($f_nombre = $_POST['f_nombre'], $f_apellidos = $_POST['f_apellidos'], $f_correo = $_POST['f_correo'], $f_contrasena = $_POST['f_contrasena'], $f_contrasena_r = $_POST['f_contrasena_r']);
}
?>

<section class="section">
<div class="container">
<div class="card bordered z-depth-2" style="margin:0 auto; max-width:400px;">
<div class="card-header">
<span class="card-title">Registro</span>
</div>
<div class="card-content">
<?php if (isset($_GET['campos'])): ?>
<div class="alert alert-danger"><strong>Ha ocurrido un error!</strong> Faltan datos!</div>
<?php elseif (isset($_GET['correo'])): ?>
<div class="alert alert-danger"><strong>Ha ocurrido un error!</strong> El correo electronico ya esta registrado o no ha ingresado alguno!</div>
<?php elseif (isset($_GET['error'])): ?>
<div class="alert alert-danger"><strong>Ha ocurrido un error!</strong> Intentalo nuevamente!</div>
<?php endif ?>
<form role="form" method="POST" action="registro">
<div class="input-group">
<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
<input type="text" class="form-control" placeholder="Nombre(s)" id="f_nombre" name="f_nombre" required="">
</div>
<br/>
<div class="input-group">
<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
<input type="text" class="form-control" placeholder="Apellido(s)" id="f_apellidos" name="f_apellidos" required="">
</div>
<br/>
<div class="input-group">
<span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
<input type="email" class="form-control" placeholder="Correo" id="f_correo" name="f_correo" required="">
</div>
<br/>
<div class="input-group">
<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
<input type="password" class="form-control" placeholder="Contraseña" name="f_contrasena" required="">
</div>
<br/>
<div class="input-group">
<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
<input type="password" class="form-control" placeholder="Repetir Contraseña" name="f_contrasena_r" required="">
</div>
<br/>

<a href="ingreso"><p>¿Ya tienes cuenta?</p></a>
</div>
<div class="card-action clearfix">
<div class="pull-right">
<input type="submit" value="Registrarme" id="registro" name="registro" class="btn btn-default">
</div>
</div>
</form>
</div>
</div>
</section>

</body>
</html>