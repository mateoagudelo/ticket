<?php require_once('vistas/vista.encabezado.php'); require_once('controladores/c.funciones.php'); general::verificar_usuario(); general::salir(); ?>

<div class="container" style="margin-top: 25px;">

<div class="row">
<div class="col-md-12">
<div class="panel panel-default">
<div class="panel-body">
<center><h4>¿Esta segur@ que desea cerrar su cuenta?</h4></center>
<center>
<form method="POST" action="salir"><div class="btn-group">
<input type="submit" id="no" name="no" class="btn btn-success center-block" value="No, volver.">
<input type="submit" id="salir" name="salir" class="btn btn-danger center-block" value="Si, salir.">
</div></form></p></center>

</div>
</div>
</div>

</div>
</div>
<?php require_once('vistas/vista.src.php'); ?>