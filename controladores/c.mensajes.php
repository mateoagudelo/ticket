<?php

class Mensajes {
	protected $con, $consultar, $consultar2, $entradas;
	private $datos, $get_mensajes, $id_tiquete, $mensaje, $id_usuario;
	public $get_nombre_usuario;

	public function __construct($id) {
		$this->id_tiquete = $id;
		require_once('app/inc/clase.conectar.php');
		$this->con = new Conectar();
	}

	public function mensaje_usuario($id_usuario, $mensaje) {
		$this->mensaje = $mensaje;
		$this->id_usuario = $id_usuario;
		$this->entradas = $this->con->prepare('INSERT INTO t_mensajes SET id_tiquete=:id_tiquete, id_usuario=:id_usuario, mensaje_usuario=:mensaje');
		$this->entradas->execute( array(':id_tiquete' => $this->id_tiquete, ':id_usuario' => $this->id_usuario, ':mensaje' => $this->mensaje) );
		header("Location: ticket?id=$this->id_tiquete");
	}

	public function get_mensajes() {
		$this->consultar = $this->con->prepare('SELECT * FROM t_mensajes WHERE id_tiquete=:id');
		$this->consultar->execute( array(':id' => $this->id_tiquete) );

		while ($this->get_mensajes = $this->consultar->fetch(PDO::FETCH_OBJ)) {
			if ($this->get_mensajes->id_super == NULL) {
			$this->consultar2 = $this->con->prepare('SELECT * FROM t_usuarios WHERE id_usuario=:id');
			$this->consultar2->execute( array(':id' => $this->get_mensajes->id_usuario) );
			$this->consultar2 = $this->consultar2->fetch(PDO::FETCH_OBJ);
			
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading"><h4><strong>' . $this->consultar2->nombre_usuario . '</strong></h4></div>';
			echo '<div class="panel-body">';

			echo $this->get_mensajes->mensaje_usuario;

			echo '</div>';
			echo '</div>';

			} elseif ($this->get_mensajes->id_usuario == NULL) {
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading"><h4><strong>' . 'Admin' . '</strong></h4></div>';
			echo '<div class="panel-body">';

			echo $this->get_mensajes->mensaje_super;

			echo '</div>';
			echo '</div>';
		} 

		}
	}

}

?>