<?php

class general {

    public function base_url() {
        $recurso = $_SERVER['REQUEST_URI'];
        $separar = explode(".",$recurso);
        $separar = end($separar); 

        if($separar == "php"){
        $dir =substr("$recurso", 0, -4); //
        header("Location: $dir");
        } else{}
    }

    public function verificar_usuario() {
        session_start();
        if(!$_SESSION['id_usuario_sistema'] AND !$_SESSION['correo_usuario_sistema']){
        session_destroy();
        header('location: ingreso');
        } else {}
    }

    public function verificar_entrada() {
        session_start();
        if(isset($_SESSION['id_usuario_sistema']) AND isset($_SESSION['correo_usuario_sistema'])){
        header('location: index');
        } else {}
    }

    public function salir() {
        if (isset($_POST['salir'])) {
        session_destroy();
        header('Location: ingreso');
        } elseif (isset($_POST['no'])) {
        header('Location: index');
        } else{} 
    }

}

?>