<?php

class Estado {
	protected $con, $consultar;
	private $id;
	public $nombre;
	public function __construct($id) {
		$this->id = $id;
		require_once('app/inc/clase.conectar.php');
		$this->con = new conectar();
		$this->consultar = $this->con->prepare('SELECT * FROM t_estados WHERE id=:id');
		$this->consultar->execute( array(':id' => $this->id) );
		$this->datos = $this->consultar->fetch(PDO::FETCH_OBJ);
		$this->nombre = $this->datos->nombre;
	}
	public function propiedad() {
		echo $this->nombre;
	}
}

require_once('app/inc/clase.conectar.php');
$con = new conectar();
$pagina = isset($_GET['pagina']) ? (int)$_GET['pagina'] : 1 ;
$postPorPagina = 5;
$inicio = ($pagina > 1) ? ($pagina * $postPorPagina - $postPorPagina) : 0;
$articulos = $con->prepare("SELECT SQL_CALC_FOUND_ROWS * FROM t_tiquetes WHERE id_creador=:id LIMIT $inicio, $postPorPagina");
$articulos->execute( array(':id' => $_SESSION['id_usuario_sistema']) );
$tickets = $articulos->fetchAll();

$totalArticulos = $con->query('SELECT FOUND_ROWS() as total');
$totalArticulos = $totalArticulos->fetch()['total'];
$numeroPaginas = ceil($totalArticulos / $postPorPagina);
?>
