<?php 

class recuperar {
	private $con, $consultar, $f_correo, $correo, $datos;
	public $ve_correo, $si_correo;
	protected $codigo;

	public function __construct($f_correo) {
		$this->f_correo = $f_correo;
		if (!empty($this->f_correo)) {
			$this->f_correo = trim($this->f_correo);
			$this->ve_correo = true;
		} else {
			$this->ve_correo = false;
		}	
	}

	public function verificar_correo() {
		if ($this->ve_correo) {
		require_once('app/inc/clase.conectar.php');
		$this->con = new Conectar();
		$this->consultar = $this->con->prepare('SELECT * FROM t_usuarios WHERE correo_usuario=:correo');
		$this->consultar->execute( array(':correo' => $this->f_correo) );
		$this->datos = $this->consultar->fetch(PDO::FETCH_OBJ);
		$this->correo = $this->datos->correo_usuario;

		if ($this->f_correo == $this->correo) {
			$this->si_correo = true;
		} else {
			$this->si_correo = false;
		}

		} else { header('Location: r-paso1?error=correo'); }
	}

	public function generar_codigo() {
		if ($this->si_correo) {
			$this->codigo = rand(20000, 99999999);
			$this->consultar = $this->con->prepare('UPDATE t_usuarios SET codigo_recuperar_contrasena=:codigo WHERE correo_usuario=:correo');
			$this->consultar->execute( array(':codigo' => $this->codigo, ':correo' => $this->correo) );
			mail($this->f_correo, "Codigo de recuperación", "Tu codigo es el siguiente: " . $this->codigo);
			header('Location: r-paso2');
		} else {
			header('Location: r-paso1?error=correo');
		}	
	}
}

class confirmar {
	private $correo, $codigo, $con, $consultar, $datos, $si_todo;
	public $si_correo, $si_codigo;

	public function verificar_datos($correo, $codigo) {
		$this->correo = $correo;
		$this->codigo = $codigo;

		if (!empty($this->correo)) {
			$this->correo = trim($this->correo);
			$this->si_correo = true;
		} else {
			$this->si_correo = false;
		}
		
		if (!empty($this->codigo)) {
			$this->codigo = trim($this->codigo);
			$this->si_codigo = true;
		} else {
			$this->si_codigo = false;
		}
	}

	public function consultar_datos() {
		if ($this->si_correo AND $this->si_codigo) {
			require_once('app/inc/clase.conectar.php');
			$this->con = new conectar();
			$this->consultar = $this->con->prepare('SELECT * FROM t_usuarios WHERE correo_usuario=:correo');
			$this->consultar->execute( array(':correo' => $this->correo) );
			$this->datos = $this->consultar->fetch(PDO::FETCH_OBJ);

			if ($this->datos->correo_usuario == $this->correo AND $this->datos->codigo_recuperar_contrasena == $this->codigo) {
				session_start();
				$_SESSION['codigo_usuario_recuperar'] = $this->datos->codigo_recuperar_contrasena;
				$_SESSION['correo_usuario_recuperar'] = $this->datos->correo_usuario;
				$_SESSION['id_usuario_recuperar'] = $this->datos->id_usuario;
				header('Location: r-paso3');
			} else {
				session_destroy();
				header('Location: r-paso1?error1');
			}
		} else {
			header('Location: r-paso1?error2');
		}
	}
}

class cambiar_contrasena {
	private $con, $consultar, $datos, $contrasena, $r_contrasena, $si_contrasena, $si_rcontrasena, $si_todo, $codigo, $codigo_verificado;
	public function __construct($codigo, $id, $contrasena, $r_contrasena) {
		$this->codigo = $codigo;
		$this->id = $id;
		$this->contrasena = $contrasena;
		$this->r_contrasena = $r_contrasena;
		if (!empty($this->contrasena) AND !empty($this->r_contrasena)) {
			$this->si_contrasena = true;
			$this->si_rcontrasena = true;
		} else {
			$this->si_contrasena = false;
			$this->si_rcontrasena = false;
		}
		require_once('app/inc/clase.conectar.php');
		$this->con = new conectar();
	}

	public function verificar() {
		if ($this->si_contrasena AND $this->si_rcontrasena) {
			$this->consultar = $this->con->prepare('SELECT * FROM t_usuarios WHERE codigo_recuperar_contrasena=:codigo');
			$this->consultar->execute( array(':codigo' => $this->codigo) );
			$this->datos = $this->consultar->fetch(PDO::FETCH_OBJ);
			$this->codigo_verificado = $this->datos->codigo_recuperar_contrasena;

			if ($this->contrasena == $this->r_contrasena AND $this->codigo_verificado == $this->codigo) {
			$this->si_todo = true;
			} else { $this->si_todo = false; }
		} else {
			$this->si_todo = false;
		}	
	}
	public function cambiar() {
		if ($this->si_todo) {
			$this->contrasena = password_hash($this->contrasena, PASSWORD_DEFAULT, array("cost"=>12));
			$this->consultar = $this->con->prepare('UPDATE t_usuarios SET contrasena_usuario=:contrasena WHERE id_usuario=:id');
			$this->consultar->execute( array(':contrasena' => $this->contrasena, ':id' => $this->id) );
			self::cambiar_codigo();
			session_destroy();
			header('Location: index');
		} else {
			header('Location: r-paso3?error');
		}	
	}

	public function cambiar_codigo() {
		$this->consultar = $this->con->prepare('UPDATE t_usuarios SET codigo_recuperar_contrasena=:codigo WHERE id_usuario=id');
		$this->consultar->execute( array(':codigo' => null, ':id' => $this->id) );
	}
}

?>