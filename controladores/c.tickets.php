<?php

class Ticket {
	protected $con, $consultar, $id, $ticket, $get_estado_datos, $get_departamento_datos, $get_prioridad_datos, $get_producto_datos;
	private $id_verificado, $terminos, $id_estado, $id_departamento, $id_prioridad, $id_producto, $url_archivo_access, $id_eliminar_archivo;
	public $numero, $nombre, $descripcion, $creado, $estado, $departamento, $prioridad, $producto, $url, $archivo;

	public function __construct($autor, $id) {
		require_once('app/inc/clase.conectar.php');
		$this->con = new conectar();
		$this->autor = $autor;
		$this->id = $id;
	}

	public function Verificar_entrada() {
		if (empty($this->id)) {
			header('Location: index');
		} else {
			$this->terminos = array('UPDATE', 'SELECT', 'DELETE', 'or', 'OR', 'AND', 'and', '&');
			$this->id = str_replace($this->terminos, '', $this->id);
			return $this->id;
		}
	}

	private function get_estado($estado) {
		$this->id_estado = $estado;
		$this->consultar = $this->con->prepare('SELECT id, nombre FROM t_estados WHERE id=:estado');
		$this->consultar->execute( array(':estado' => $this->id_estado) );
		$this->get_estado_datos = $this->consultar->fetch(PDO::FETCH_OBJ);
		$this->estado = $this->get_estado_datos->nombre;
	}

	private function get_departamento($departamento) {
		$this->id_departamento = $departamento;
		$this->consultar = $this->con->prepare('SELECT id, nombre FROM t_departamentos WHERE id=:departamento');
		$this->consultar->execute( array(':departamento' => $this->id_departamento) );
		$this->get_departamento_datos = $this->consultar->fetch(PDO::FETCH_OBJ);
		$this->departamento = $this->get_departamento_datos->nombre;
	}

	private function get_prioridad($prioridad) {
		$this->id_prioridad = $prioridad;
		$this->consultar = $this->con->prepare('SELECT id, nombre FROM t_prioridad WHERE id=:prioridad');
		$this->consultar->execute( array(':prioridad' => $this->id_prioridad) );
		$this->get_prioridad_datos = $this->consultar->fetch(PDO::FETCH_OBJ);
		$this->prioridad = $this->get_prioridad_datos->nombre;
	}

	private function get_producto($producto) {
		$this->id_producto = $producto;
		$this->consultar = $this->con->prepare('SELECT id, nombre FROM t_productos WHERE id=:producto');
		$this->consultar->execute( array(':producto' => $this->id_producto) );
		$this->get_producto_datos = $this->consultar->fetch(PDO::FETCH_OBJ);
		$this->producto = $this->get_producto_datos->nombre;
	}

	public function get_datos() {
		$this->consultar = $this->con->prepare('SELECT * FROM t_tiquetes WHERE id_tiquete=:id');
		$this->consultar->execute( array(':id' => $this->id) );
		$this->ticket = $this->consultar->fetch(PDO::FETCH_OBJ);

		if ($this->autor != $this->ticket->id_creador) {
			header('Location: index');
		}
		$this->archivo = $this->ticket->url_archivo;
		$this->id_ticket = $this->ticket->id_tiquete;
		$this->numero = $this->ticket->numero;
		$this->nombre = $this->ticket->nombre;
		$this->descripcion = $this->ticket->descripcion;
		$this->creado = $this->ticket->fecha;
		$this->estado_verificado = $this->ticket->id_estado;
		self::get_estado($this->ticket->id_estado);
		self::get_departamento($this->ticket->id_departamento);
		self::get_prioridad($this->ticket->id_prioridad);
		self::get_producto($this->ticket->id_producto);
	}
	public function Id_ticket() {
		echo (int)$this->id_ticket;
	}
	public function Numero() {
		echo (int)$this->numero;
	}
	public function Nombre() {
		echo htmlspecialchars($this->nombre);
	}
	public function Descripcion() {
		echo $this->descripcion;
	}
	public function Creado() {
		echo htmlspecialchars($this->creado);
	}
	public function Estado() {
		echo htmlspecialchars($this->estado);
	}
	public function Departamento() {
		echo htmlspecialchars($this->departamento);
	}
	public function Prioridad() {
		echo htmlspecialchars($this->prioridad);
	} 
	public function Producto() {
		echo htmlspecialchars($this->producto);
	}
	public function Archivo() {
		echo $this->archivo;
	}


	public function cerrar_estado() {
		if ($this->autor != $this->ticket->id_creador) {
			header('Location: index');
		} else {
		$this->consultar = $this->con->prepare('UPDATE t_tiquetes SET id_estado=:estado WHERE id_tiquete=:id');
		$this->consultar->execute( array(':estado' => 3, ':id' => $this->id) );
		header("Location: ticket?id=$this->id");
		}
	}

	public function get_url_archivo($id) {
		$this->id_eliminar_archivo = $id;
		$this->consultar = $this->con->prepare('SELECT * FROM t_tiquetes WHERE id_tiquete=:id');
		$this->consultar->execute( array(':id' => $this->id_eliminar_archivo) );
		$this->url_archivo_access = $this->consultar->fetch(PDO::FETCH_OBJ); 
	}

	public function eliminar_ticket() {
		if ($this->autor != $this->ticket->id_creador) {
			header('Location: index');
		} else {
		self::get_url_archivo($this->id);
		opendir('app/archivos/');
		unlink($this->url_archivo_access->url_archivo);
		$this->consultar = $this->con->prepare('DELETE FROM t_tiquetes WHERE id_tiquete=:id');
		$this->consultar->execute( array(':id' => $this->id) );
		header("Location: index");
		}
	}

	public function __destruct() {
		$this->con = null;
		$this->consultar = null;
	}
}


?>