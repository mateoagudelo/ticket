<?php

class perfil {
	private $con;
	protected $id, $nombre, $apellidos, $correo;
	public $consultar, $datos;
	private $id_session;
	public function __construct($id_session) {
		require_once('app/inc/clase.conectar.php');
		$this->con = new Conectar();
		$this->id_session = $id_session;
		$this->consultar = $this->con->prepare('SELECT * FROM t_usuarios WHERE id_usuario=:id');
		$this->consultar->execute( array(':id' => $this->id_session) );
		$this->datos = $this->consultar->fetch(PDO::FETCH_OBJ);
		$this->id = $this->datos->id_usuario;
		$this->nombre = $this->datos->nombre_usuario;
		$this->apellidos = $this->datos->apellidos_usuario;
		$this->correo = $this->datos->correo_usuario;
	}
	public function nombre() {
		return htmlspecialchars($this->nombre);
	}
	public function apellidos() {
		return htmlspecialchars($this->apellidos);
	}
	public function correo() {
		return htmlspecialchars($this->correo);
	}
}

class actualizar_datos {
	private $con, $consultar, $datos, $id_session, $datos_igualdad;
	public $nombre, $si_nombre, $apellidos, $si_apellidos;
	public function __construct($id_session, $f_nombre, $f_apellidos) {
		require_once('app/inc/clase.conectar.php');
		$this->con = New Conectar();
		$this->nombre = $f_nombre;
		$this->apellidos = $f_apellidos;
		$this->id_session = $id_session;
	}
	public function verificar_datos() {
		if (!empty($this->nombre)) {
			$this->nombre = filter_var($this->nombre, FILTER_SANITIZE_STRING);
			$this->nombre = trim($this->nombre);
			$this->nombre = addslashes($this->nombre);
			$this->nombre = htmlspecialchars($this->nombre);
			$this->si_nombre = true;
		} else {
			$this->si_nombre = false;
		}
		if (!empty($this->apellidos)) {
			$this->apellidos = filter_var($this->apellidos, FILTER_SANITIZE_STRING);
			$this->apellidos = trim($this->apellidos);
			$this->apellidos = addslashes($this->apellidos);
			$this->apellidos = htmlspecialchars($this->apellidos);
			$this->si_apellidos = true;
		} else {
			$this->si_apellidos = false;
		}
	}
	public function verificar_igualadad() {
		$this->consultar = $this->con->prepare('SELECT * FROM t_usuarios WHERE id_usuario=:id');
		$this->consultar->execute( array(':id' => $this->id_session) );
		$this->datos_igualdad = $this->consultar->fetch(PDO::FETCH_OBJ);

		if ($this->datos_igualdad->nombre_usuario == $this->nombre AND $this->datos_igualdad->apellidos_usuario == $this->apellidos) {
			$this->si_nombre = false;
			$this->si_apellidos = false;
		}
	}

	public function actualizar() {
		if ($this->si_nombre == true AND $this->si_apellidos == true) {
				$this->consultar = $this->con->prepare('UPDATE t_usuarios SET nombre_usuario=:nombre, apellidos_usuario=:apellidos WHERE id_usuario=:id');
				$this->consultar->execute( array(':nombre' => $this->nombre, ':apellidos' => $this->apellidos,':id' => $this->id_session) );
				header('Location: perfil');
		} else { header('Location: perfil?error'); }
	}

	public function __destruct() {
		$this->con = null;
		$this->consultar = null;
	}
}

class actualizar_contrasena {
	protected $id, $consultar, $con;
	public $contrasena_actual, $con_a_si;
	public $contrasena_nueva, $con_n_si, $contrasena_nueva_rep, $con_n_r_si;
	private $datos;

	public function __construct($id, $con_a, $con_n, $con_n_r) {
		require_once('app/inc/clase.conectar.php');
		$this->con = new Conectar();
		$this->id = $id;
		$this->contrasena_actual = $con_a;
		$this->contrasena_nueva = $con_n;
		$this->contrasena_nueva_rep = $con_n_r;
	}

	public function verificar_datos() {
		if (!empty($this->contrasena_actual)) {
			$this->con_a_si = true;
		} else {
			$this->con_a_si = false;
		}

		if (!empty($this->contrasena_nueva)) {
			$this->con_n_si = true;
		} else {
			$this->con_n_si = false;
		}

		if (!empty($this->contrasena_nueva_rep)) {
			$this->con_n_r_si = true;
		} else {
			$this->con_n_r_si = false;
		}
	}

	public function actualizar() {
		$this->consultar = $this->con->prepare('SELECT * FROM t_usuarios WHERE id_usuario=:id');
		$this->consultar->execute( array(':id' => $this->id) );
		$this->datos = $this->consultar->fetch(PDO::FETCH_OBJ);

		if (password_verify($this->contrasena_actual, $this->datos->contrasena_usuario)) {
			if ($this->contrasena_nueva == $this->contrasena_nueva_rep) {
				//ACTUALIZAR
				$this->contrasena_nueva = password_hash($this->contrasena_nueva, PASSWORD_DEFAULT, array("cost"=>12));
				$this->consultar = $this->con->prepare('UPDATE t_usuarios SET contrasena_usuario=:contrasena WHERE id_usuario=:id');
				$this->consultar->execute( array(':contrasena' => $this->contrasena_nueva, ':id' => $this->id) );
				session_destroy();

				session_start();
				$_SESSION['contrasena_actualizada'] = rand(1, 9999);
				header('Location: contrasena-actualizada');
			} else { header('Location: perfil?n'); }

		} else { header('Location: perfil?a'); }
	}

	public function __destruct() {
		$this->con = null;
		$this->consultar = null;
	}

}

?>