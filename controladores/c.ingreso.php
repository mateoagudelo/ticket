<?php

class ingreso {
	public $f_correo, $f_contrasena;
	private $datos, $consultar, $con, $si_correo, $si_contrasena;
	protected $correo_verificado, $contrasena_verificada, $id_verificado;

	public function __construct(){
		require_once('app/inc/clase.conectar.php');
		$this->con = new Conectar();
	}

	public function verificar_datos($f_correo, $f_contrasena) {
		$this->f_correo = $f_correo;
		$this->consultar = $this->con->prepare('SELECT * FROM t_usuarios WHERE correo_usuario=:correo');
		$this->consultar->execute( array(':correo' => $this->f_correo) );
		$this->datos = $this->consultar->fetch(PDO::FETCH_OBJ);
		$this->correo_verificado = $this->datos->correo_usuario;

		if ($this->correo_verificado = $this->f_correo) {
			$this->si_correo = true;
			$this->contrasena_verificada = $this->datos->contrasena_usuario;
		} else {
			$this->si_correo = false;
		}

		$this->f_contrasena = $f_contrasena;

		if (password_verify($this->f_contrasena, $this->contrasena_verificada)) {
			$this->si_contrasena = true;
			$this->id_verificado = $this->datos->id_usuario;
		} else {
			$this->si_contrasena = false;
		}
	}

	public function verificar() {
		if ($this->si_correo == true AND $this->si_contrasena == true) {
			if (!is_null($this->datos->codigo_recuperar_contrasena)) {
				$this->consultar = $this->con->prepare('UPDATE t_usuarios SET codigo_recuperar_contrasena=:limpiar WHERE correo_usuario=:correo');
				$this->consultar->execute( array(':limpiar' => NULL, ':correo' => $this->f_correo) );
			}
			session_start();
			$_SESSION['id_usuario_sistema'] = $this->id_verificado;
			$_SESSION['correo_usuario_sistema'] = $this->correo_verificado;
			header('Location: index');
		} else {
			header('Location: ingreso');
		}
	}
}
?>