<?php
class registro {
	public $nombre, $apellidos, $correo, $contrasena, $r_contrasena, $errores, $v_nombre, $v_apellidos, $v_correo, $v_contrasena;
	protected $con, $consultar;
	//VERIFICADORES
	public $consultar_correo, $datos_consulta_correo, $correo_consultado;

	public function __construct ($nombre, $apellidos, $correo, $contrasena, $r_contrasena) {
		require_once('app/inc/clase.conectar.php');
		$this->con = new Conectar();
		$this->nombre = $nombre;
		$this->apellidos = $apellidos;
		$this->correo = $correo;
		$this->contrasena = $contrasena;
		$this->r_contrasena = $r_contrasena;

		if (!empty($this->nombre)) {
			$this->nombre = filter_var($this->nombre, FILTER_SANITIZE_STRING);
			$this->nombre = addslashes($this->nombre);
			$this->nombre = htmlspecialchars($this->nombre);
			$this->v_nombre = true;
		} else {
			$this->v_nombre = false;
			header('Location: registro?campos');
		}

		if (!empty($this->apellidos)) {
			$this->apellidos = filter_var($this->apellidos, FILTER_SANITIZE_STRING);
			$this->apellidos = addslashes($this->apellidos);
			$this->apellidos = htmlspecialchars($this->apellidos);
			$this->v_apellidos = true;
		} else {
			$this->v_apellidos = false;
			header('Location: registro?campos');
		}

		if (!empty($this->correo)) {
			$this->correo = filter_var($this->correo, FILTER_SANITIZE_EMAIL);
			if (filter_var($this->correo, FILTER_VALIDATE_EMAIL)) {
				$this->correo = trim($this->correo);
				$this->correo = strtolower($this->correo);
				$this->v_correo = true;
			} else {
				$this->v_correo = false;
				header('Location: registro?campos');
			}
		} else {
			header('Location: registro?campos');
		}

		if (!empty($this->contrasena)) {
			if ($this->contrasena == $this->r_contrasena) {
				$this->contrasena = password_hash($this->contrasena, PASSWORD_DEFAULT, array("cost"=>12)); //password_hash($this->contrasena, PASSWORD_DEFAULT, array("cost"=>12));
				$this->v_contrasena = true;
			} else {
				$this->v_contrasena = false;
			}
		} else {
			$this->v_contrasena = false;
			header('Location: registro?campos');
		}

		$this->consultar_correo = $this->con->prepare('SELECT * FROM t_usuarios WHERE correo_usuario=:correo_duplicado');
		$this->consultar_correo->execute( array(':correo_duplicado' => $this->correo) );
		$this->datos_consulta_correo = $this->consultar_correo->fetch(PDO::FETCH_OBJ);
		$this->correo_consultado = $this->datos_consulta_correo->correo_usuario;

		if($this->v_nombre == true AND $this->v_apellidos == true AND $this->v_correo == true AND $this->v_contrasena == true) {

		if ($this->correo == $this->correo_consultado) {
		header('Location: registro?correo');
		} else {
		try {
		$this->consultar =  $this->con->prepare('INSERT INTO t_usuarios SET nombre_usuario=:nom, apellidos_usuario=:ape, correo_usuario=:cor, contrasena_usuario=:con');
		$this->consultar->execute( array(':nom' => $this->nombre, ':ape' => $this->apellidos, ':cor' => $this->correo, ':con'=> $this->contrasena) );
		header('Location: ingreso');
		} catch (PDOException $error_insertar) {
			echo $error_insertar->getMessage();
		}
		} //FN

	} else {
		header('Location: registro?error');
	}


	}
}
?>