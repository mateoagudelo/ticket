<?php

class Seccion {
	protected $con, $consultar;
	private $departamentos, $productos;
	public function __construct(){
		require_once('app/inc/clase.conectar.php');
		$this->con = new Conectar();
	}
	public function Departamentos() {
			$this->consultar = $this->con->prepare('SELECT * FROM t_departamentos');
			$this->consultar->execute();
		while ($this->departamentos = $this->consultar->fetch(PDO::FETCH_OBJ)) {
			echo '<tr>';
			echo '<td>' . $this->departamentos->nombre . '</td>';
			echo '<td>' . $this->departamentos->descripcion . '</td>';
			echo '</tr>';
		}
	}
	public function Productos() {
		$this->consultar = $this->con->prepare('SELECT * FROM t_productos');
		$this->consultar->execute();
		while ($this->productos = $this->consultar->fetch(PDO::FETCH_OBJ)) {
			echo '<tr>';
			echo '<td>' . $this->productos->nombre . '</td>';
			echo '<td>' . $this->productos->descripcion . '</td>';
			echo '</tr>';
		}
	}
}

?>