<?php

class Seleccion {
	protected $con, $consultar, $departamentos, $productos, $prioridad;
	public function __construct() {
		require_once('app/inc/clase.conectar.php');
		$this->con = new Conectar();
	}
	public function departamentos() {
		$this->consultar = $this->con->prepare('SELECT * FROM t_departamentos');
		$this->consultar->execute();
		echo '<select class="form-control" id="departamento" name="departamento">';
		echo '<option value="0" selected>' . htmlspecialchars('-- Seleccionar --') .'</option>';
		while ($this->departamentos = $this->consultar->fetch(PDO::FETCH_OBJ)) {
			echo '<option value=' . htmlspecialchars($this->departamentos->id) . '>' . $this->departamentos->nombre . '</option>';
		}
		echo '</select>';
	}

	public function productos() {
		$this->consultar = $this->con->prepare('SELECT * FROM t_productos');
		$this->consultar->execute();
		echo '<select class="form-control" id="producto" name="producto">';
		echo '<option value="0" selected>' . htmlspecialchars('-- Seleccionar --') .'</option>';
		while ($this->productos = $this->consultar->fetch(PDO::FETCH_OBJ)) {
			echo '<option value=' . htmlspecialchars($this->productos->id) . '>' . $this->productos->nombre . '</option>';
		}
		echo '</select>';
	}

	public function prioridad() {
		$this->consultar = $this->con->prepare('SELECT * FROM t_prioridad');
		$this->consultar->execute();
		echo '<select class="form-control" id="prioridad" name="prioridad">';
		echo '<option value="0" selected>' . htmlspecialchars('-- Seleccionar --') .'</option>';
		while ($this->prioridad = $this->consultar->fetch(PDO::FETCH_OBJ)) {
			echo '<option value=' . htmlspecialchars($this->prioridad->id) . '>' . $this->prioridad->nombre . '</option>';
		}
		echo '</select>';
	}
}

class Crear {
	protected $con, $consultar, $datos, $datos_departamentos, $datos_productos, $datos_prioridad;
	private $id_creador, $nombre, $descripcion, $departamento, $producto, $prioridad, $fecha, $num1, $num2, $numero;
	public $departamento_verificado, $producto_verificado, $prioridad_verificada, $si_nombre, $si_descripcion, $si_departamento, $si_producto, $si_prioridad;

	//Archivos
	public $archivo_nombre, $tipo, $tamano, $stored, $error, $v_tamano, $directorio, $destino;
	public function __construct($id, $nombre, $descripcion, $departamento, $producto, $prioridad) {
		$this->id_creador = $id;
		$this->nombre = $nombre;
		$this->descripcion = $descripcion;
		$this->departamento = $departamento;
		$this->producto = $producto;
		$this->prioridad = $prioridad;
		$this->fecha = date("d") . " del " . date("m") . " de " . date("Y");
		if (!empty($this->nombre)) {
			$this->si_nombre = true;
		} else { $this->si_nombre = false; }

		if (!empty($this->descripcion)) {
			$this->si_descripcion = true;
		} else { $this->si_descripcion = false; }

		if (!empty($this->departamento)) {
			$this->si_departamento = true;
		} else { $this->si_departamento = false; }

		if (!empty($this->producto)) {
			$this->si_producto = true;
		} else { $this->si_producto = false; }

		if (!empty($this->prioridad)) {
			$this->si_prioridad = true;
		} else { $this->si_prioridad = false; }		

		require_once('app/inc/clase.conectar.php');
		$this->con = new Conectar();

		$this->num1 = rand(999, 99999);
		$this->num2 = rand(999, 99999);
		$this->numero = $this->num1 + $this->num2;
	}

	public function verificar_departamento() {
		if ($this->si_departamento) {
		$this->consultar = $this->con->prepare('SELECT * FROM t_departamentos WHERE id=:id');
		$this->consultar->execute( array(':id' => $this->departamento) );
		$this->datos_departamentos = $this->consultar->fetch(PDO::FETCH_OBJ);

		if ($this->datos_departamentos->id == $this->departamento) {
			$this->departamento_verificado = true;
		} else {
			$this->departamento_verificado = false;
		}
		
	   	} else { 
	   		$this->departamento_verificado = false;
	   	}
	}

	public function verificar_producto() {
		if ($this->si_producto) {
		$this->consultar = $this->con->prepare('SELECT * FROM t_productos WHERE id=:id');
		$this->consultar->execute( array(':id' => $this->producto) );
		$this->datos_productos = $this->consultar->fetch(PDO::FETCH_OBJ);

		if ($this->datos_productos->id == $this->producto) {
			$this->producto_verificado = true;
		} else {
			$this->producto_verificado = false;
		}
		
		} else { 
			$this->producto_verificado = false;
		}
		
	}

	public function verificar_prioridad() {
		if ($this->si_prioridad) {
		$this->consultar = $this->con->prepare('SELECT * FROM t_prioridad WHERE id=:id');
		$this->consultar->execute( array(':id' => $this->prioridad) );
		$this->datos_prioridad = $this->consultar->fetch(PDO::FETCH_OBJ);

		if ($this->datos_prioridad->id == $this->prioridad) {
			$this->prioridad_verificada = true;
		} else {
			$this->prioridad_verificada = false;
		}
			
		} else { 
			$this->prioridad_verificada = false;
		}
		
	}

	public function Upload() {
		$this->archivo_nombre = $_FILES['archivo_sistema']['name'];
		$this->tipo = $_FILES['archivo_sistema']['type'];
		$this->tamano = $_FILES['archivo_sistema']['size'];
		$this->stored = $_FILES['archivo_sistema']['tmp_name'];
		$this->error = $_FILES['archivo_sistema']['error'];
		$this->directorio = "app/archivos/";
		$this->destino = $this->directorio.$this->archivo_nombre;

		if ($this->tamano > 2000000) {
			$this->v_tamano = false;
		} else {
			$this->v_tamano = true;
		}

		if ($this->v_tamano) {
		opendir($this->directorio);
		copy($this->stored, $this->destino);
		echo "<img src=\"$name\">";
		} else {
			$this->subida = false;
		}
	}

	public function insertar_datos() {
		self::verificar_departamento();
		self::verificar_producto();
		self::verificar_prioridad();
		if ($this->si_nombre AND $this->si_descripcion AND $this->departamento_verificado AND $this->producto_verificado AND $this->prioridad_verificada) {
			self::Upload();
			$this->consultar = $this->con->prepare('INSERT INTO t_tiquetes SET id_creador=:id, numero=:num, fecha=:fecha, nombre=:nombre, descripcion=:descripcion, id_departamento=:id_depar, id_prioridad=:prioridad, id_producto=:id_prod, url_archivo=:archivo');
			$this->consultar->execute( array(':id' => $this->id_creador, ':num' => $this->numero,':fecha' => $this->fecha, ':nombre' => $this->nombre, ':descripcion' => $this->descripcion, ':id_depar' => $this->departamento, ':prioridad' => $this->prioridad, ':id_prod' => $this->producto, ':archivo' => $this->destino) );
			
			header('Location: crear');
		} else {
			header('Location: crear?error');
		}	
	}

	public function __destruct() {
		$this->con = null;
		$this->consultar = null;
	}
}

?>