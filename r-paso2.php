<?php require_once('vistas/vista.encabezado.php'); require_once('controladores/c.funciones.php'); general::base_url(); general::verificar_entrada(); require_once('controladores/c.ingreso.php'); require_once('controladores/c.recuperar.php'); ?>

<?php 
if (isset($_POST['confirmar_datos'])) {
	$confirmar = new confirmar();
	$confirmar->verificar_datos($c_correo = $_POST['c_correo'], $c_codigo = $_POST['c_codigo']);
	$confirmar->consultar_datos();
}
?>

<section class="section">
<div class="container">
<div class="card bordered z-depth-2" style="margin:0 auto; max-width:400px;">
<div class="card-header">
<span class="card-title">Paso 2: Confirmar datos</span>
</div>
<div class="card-content">
<form role="form" method="POST" action="r-paso2">
<div class="input-group">
<span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
<input type="text" class="form-control" placeholder="Correo" id="c_correo" name="c_correo" required="" autocomplete="off">
</div>
</br>
<div class="input-group">
<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
<input type="password" class="form-control" placeholder="Codigo" id="c_codigo" name="c_codigo" required="">
</div>
</div>

<div class="card-action clearfix">
<div class="pull-right">
<input type="submit" id="confirmar_datos" name="confirmar_datos" value="Confirmar" class="btn btn-success">
</div>
</div>
</form>
</div>
</div>
</section>
</body>
</html>