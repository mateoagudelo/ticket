<?php require_once('vistas/vista.encabezado.php'); require_once('controladores/c.funciones.php'); general::base_url(); general::verificar_entrada(); require_once('controladores/c.ingreso.php'); ?>



<section class="section">
<div class="container">
<div class="card bordered z-depth-2" style="margin:0 auto; max-width:400px;">
<div class="card-header">
<span class="card-title">Recuperar contraseña</span>
</div>
<div class="card-content">
<form role="form" method="POST" action="recuperar">
<div class="input-group">
<span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
<input type="text" class="form-control" placeholder="Correo" id="f_correo" name="f_correo" required="">
</div>
</div>
<div class="card-action clearfix">
<div class="pull-right">
<input type="submit" id="recuperar_correo" name="recuperar_correo" value="Solicitar" class="btn btn-success">
</div>
</div>
</form>
</div>
</div>
</section>


</body>
</html>