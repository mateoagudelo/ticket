<nav class="navbar navbar-default navbar-fixed-top z-depth-1" role="navigation">
<div class="container-fluid">
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
<span class="sr-only">Desplegar navegación</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="#">Logotipo</a>
</div>
<div class="collapse navbar-collapse navbar-ex1-collapse">
<ul class="nav navbar-nav">
<li class="dropdown">
<a href="#" class="dropdown-toggle" data-toggle="dropdown"> Tiquetes <b class="caret"></b>
</a>
<ul class="dropdown-menu">
<li><a href="index">Todos</a></li>
<li><a href="crear">Crear nuevo</a></li>
</ul>
</li>

<li class="dropdown">
<a href="#" class="dropdown-toggle" data-toggle="dropdown"> Servicios <b class="caret"></b>
</a>
<ul class="dropdown-menu">
<li><a href="departamentos">Departamentos</a></li>
<li><a href="productos">Productos</a></li>
</ul>
</li>

</ul>
<ul class="nav navbar-nav navbar-right">
<li class="dropdown">
<a href="#" class="dropdown-toggle" data-toggle="dropdown"> Cuenta <b class="caret"></b>
</a>

<ul class="dropdown-menu">
<li><a href="perfil">Mi cuenta</a></li>
<li class="divider"></li>
<li><a href="salir">Salir</a></li>
</ul>
</li>
</ul>
</div>
</nav>