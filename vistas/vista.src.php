<?php require_once('dir.php'); ?>
<!-- Main vendor Scripts-->
<script src="<?php echo $url_base; ?>/app/js/jquery-2.1.4.min.js"></script>
<script src="<?php echo $url_base; ?>/app/js/bootstrap.min.js"></script>
    
<!-- START Page Custom Script-->
<script src="<?php echo $url_base; ?>/app/js/mixitup.js"></script>
<script src="<?php echo $url_base; ?>/app/js/owl.carousel.js"></script>
<script src="<?php echo $url_base; ?>/app/js/parallax.js"></script>
<script src="<?php echo $url_base; ?>/app/js/swipebox.js"></script>
	
<script src="<?php echo $url_base; ?>/app/js/moment.js"></script>
<script src="<?php echo $url_base; ?>/app/js/datetimepicker.js"></script>
<script src="<?php echo $url_base; ?>/app/js/dotdotdot.min.js"></script>
<script type="text/javascript" src="<?php echo $url_base; ?>/app/js/bootstrap-filestyle.min.js"> </script>
<!-- END Page Custom Script-->
    
<!-- App Main-->
<script src="<?php echo $url_base; ?>/app/js/apps.js"></script>
<!-- END Scripts-->

</body>
</html>