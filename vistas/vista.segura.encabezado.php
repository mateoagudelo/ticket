<?php require_once('dir.php'); ?>
<!DOCTYPE html>
<html lang="es">
<head>
<!-- Required meta tags always come first -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="x-ua-compatible" content="ie=edge">    
<meta name="description" content="Bootstrap 3 Template, a sleek, intuitive, and powerful mobile first front-end framework for faster and easier web development.">
<meta name="keywords" content="HTML, CSS, JS, JavaScript, framework, bootstrap, front-end, frontend, web development">
<meta name="author" content="Werner Griesel aka Cantusstar">

<!-- CSS -->
<link rel="stylesheet" href="<?php echo $url_base; ?>/app/css/project.css">
<link rel="stylesheet" href="<?php echo $url_base; ?>/app/css/demo.css">
<link rel="stylesheet" href="<?php echo $url_base; ?>/app/css/prettify.css">
<!-- JS -->
<script type="text/javascript" src="<?php echo $url_base; ?>/app/js/prettify.js"></script>
	
<title>Sistema de soporte</title>
</head>
<body>